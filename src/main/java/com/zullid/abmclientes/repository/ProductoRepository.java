package com.zullid.abmclientes.repository;

import com.zullid.abmclientes.entity.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<Producto, Integer>{
    
}
