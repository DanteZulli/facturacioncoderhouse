package com.zullid.abmclientes.repository;

import com.zullid.abmclientes.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
    
}
