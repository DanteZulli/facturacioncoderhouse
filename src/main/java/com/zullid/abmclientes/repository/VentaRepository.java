package com.zullid.abmclientes.repository;

import com.zullid.abmclientes.entity.Venta;
import org.springframework.data.repository.CrudRepository;

public interface VentaRepository extends CrudRepository<Venta, Long>{
    
}
