package com.zullid.abmclientes.repository;

import com.zullid.abmclientes.entity.VentaProducto;
import org.springframework.data.repository.CrudRepository;

public interface VentaProductoRepository extends CrudRepository<VentaProducto, Long>{
    
}
