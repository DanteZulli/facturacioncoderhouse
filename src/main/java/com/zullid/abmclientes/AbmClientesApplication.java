package com.zullid.abmclientes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbmClientesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbmClientesApplication.class, args);
	}

}
