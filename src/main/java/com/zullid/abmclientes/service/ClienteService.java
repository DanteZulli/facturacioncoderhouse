package com.zullid.abmclientes.service;

import com.zullid.abmclientes.entity.Cliente;
import com.zullid.abmclientes.repository.ClienteRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    public String agregarCliente(Cliente nuevoCliente){
        clienteRepository.save(nuevoCliente);
        return "Nuevo Cliente Guardado!";
    }
    
    public String eliminarCliente(int id){
        clienteRepository.deleteById(id);
        return "¡Cliente Eliminado!";
    }

    public Cliente obtenerClientePorId(int id){
        Optional<Cliente> cliente = clienteRepository.findById(id);
        return cliente.orElse(null);
    }
    
    public String actualizarCliente(int id, Cliente clienteActualizado){
        Optional<Cliente> clienteExistente = clienteRepository.findById(id);

        if (clienteExistente.isPresent()) {
            Cliente cliente = clienteExistente.get();
            cliente.setNombre(clienteActualizado.getNombre());
            cliente.setEmail(clienteActualizado.getEmail());
            cliente.setTelefono(clienteActualizado.getTelefono());
            cliente.setDireccion(clienteActualizado.getDireccion());
            clienteRepository.save(cliente);
            return "¡Cliente Actualizado!";
        } else {
            return "Cliente no encontrado. No se pudo actualizar.";
        }
    }
    
    public Iterable<Cliente> obtenerTodosLosClientes(){
        return clienteRepository.findAll();
    }
}
