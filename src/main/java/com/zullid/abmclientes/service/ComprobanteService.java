/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zullid.abmclientes.service;

import com.zullid.abmclientes.entity.Cliente;
import com.zullid.abmclientes.entity.Producto;
import com.zullid.abmclientes.entity.ProductoCantidad;
import com.zullid.abmclientes.entity.Venta;
import com.zullid.abmclientes.entity.VentaProducto;
import com.zullid.abmclientes.entity.VentaRequest;
import com.zullid.abmclientes.entity.VentaResponse;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComprobanteService {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ProductoService productoService;

    @Autowired
    private VentaService ventaService;

    @Autowired
    private FechaServiceREST fechaServiceREST;

    public VentaResponse generarComprobante(VentaRequest ventaRequest) {

        Cliente cliente = clienteService.obtenerClientePorId(ventaRequest.getIdCliente());
        if (cliente == null) {
            throw new IllegalArgumentException("El cliente no existe");
        }

        for (ProductoCantidad productoCantidad : ventaRequest.getProductos()) {
            Producto producto = productoService.obtenerProductoPorId(productoCantidad.getIdProducto());
            if (producto == null) {
                throw new IllegalArgumentException("El producto con ID " + productoCantidad.getIdProducto() + " no existe");
            }
            if (producto.getStock() < productoCantidad.getCantidad()) {
                throw new IllegalArgumentException("No hay suficiente stock del producto " + producto.getNombre());
            }
        }

        Venta nuevaVenta = new Venta();
        nuevaVenta.setCliente(cliente);
        nuevaVenta.setFechaVenta(fechaServiceREST.obtenerFechaActual());
        BigDecimal totalVenta = calcularTotalVenta(ventaRequest.getProductos());
        nuevaVenta.setTotalVenta(totalVenta);
        Venta ventaGuardada = ventaService.guardarVenta(nuevaVenta);
        List<VentaProducto> ventaProductos = crearVentaProductos(ventaGuardada, ventaRequest.getProductos());
        ventaService.guardarVentaProductos(ventaProductos);

        for (VentaProducto ventaProducto : ventaProductos) {
            Producto producto = ventaProducto.getProducto();
            producto.setStock(producto.getStock() - ventaProducto.getCantidad());
            long idProductoLong = producto.getIdProducto();
            int idProductoInt = (int) idProductoLong;
            productoService.actualizarProducto(idProductoInt, producto);
        }

        imprimirComprobante(ventaGuardada, ventaProductos);

        VentaResponse ventaResponse = new VentaResponse();
        ventaResponse.setVenta(ventaGuardada);
        ventaResponse.setVentaProductos(ventaProductos);
        return ventaResponse;
    }

    private BigDecimal calcularTotalVenta(List<ProductoCantidad> productos) {
        BigDecimal totalVenta = BigDecimal.ZERO;
        for (ProductoCantidad productoCantidad : productos) {
            Producto producto = productoService.obtenerProductoPorId(productoCantidad.getIdProducto());
            BigDecimal subtotal = producto.getPrecio().multiply(BigDecimal.valueOf(productoCantidad.getCantidad()));
            totalVenta = totalVenta.add(subtotal);
        }
        return totalVenta;
    }

    private List<VentaProducto> crearVentaProductos(Venta venta, List<ProductoCantidad> productos) {
        List<VentaProducto> ventaProductos = new ArrayList<>();
        for (ProductoCantidad productoCantidad : productos) {
            Producto producto = productoService.obtenerProductoPorId(productoCantidad.getIdProducto());
            VentaProducto ventaProducto = new VentaProducto();
            ventaProducto.setVenta(venta);
            ventaProducto.setProducto(producto);
            ventaProducto.setCantidad(productoCantidad.getCantidad());
            BigDecimal subtotal = producto.getPrecio().multiply(BigDecimal.valueOf(productoCantidad.getCantidad()));
            ventaProducto.setSubtotal(subtotal);
            ventaProductos.add(ventaProducto);
        }
        return ventaProductos;
    }

    private void imprimirComprobante(Venta venta, List<VentaProducto> ventaProductos) {
        System.out.println("************* COMPROBANTE DE VENTA *************");
        System.out.println("Fecha de venta: " + venta.getFechaVenta());
        System.out.println("Cliente: " + venta.getCliente().getNombre()); // Asumiendo que el cliente tiene un nombre
        System.out.println("Total de venta: $" + venta.getTotalVenta());
        System.out.println("Productos:");

        int cantidadTotal = 0;

        for (VentaProducto ventaProducto : ventaProductos) {
            System.out.println("- " + ventaProducto.getProducto().getNombre() + ": $" + ventaProducto.getSubtotal() + " - Cantidad: " + ventaProducto.getCantidad());
            cantidadTotal += ventaProducto.getCantidad();
        }
        System.out.println("Cantidad total de productos vendidos: " + cantidadTotal);
        System.out.println("***********************************************");
    }

}
