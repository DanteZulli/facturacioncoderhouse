package com.zullid.abmclientes.service;

import com.zullid.abmclientes.entity.Producto;
import com.zullid.abmclientes.repository.ProductoRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductoService {

    @Autowired
    private ProductoRepository productoRepository;

    public String agregarProducto(Producto nuevoProducto) {
        productoRepository.save(nuevoProducto);
        return "Nuevo Producto Guardado!";
    }

    public String eliminarProducto(int id) {
        productoRepository.deleteById(id);
        return "¡Producto Eliminado!";
    }

    public Producto obtenerProductoPorId(int id) {
        Optional<Producto> producto = productoRepository.findById(id);
        return producto.orElse(null);
    }

    public String actualizarProducto(int id, Producto productoActualizado) {
        Optional<Producto> productoExistente = productoRepository.findById(id);

        if (productoExistente.isPresent()) {
            Producto producto = productoExistente.get();
            producto.setNombre(productoActualizado.getNombre());
            producto.setPrecio(productoActualizado.getPrecio());
            producto.setStock(productoActualizado.getStock());
            productoRepository.save(producto);
            return "¡Producto Actualizado!";
        } else {
            return "Producto no encontrado. No se pudo actualizar.";
        }
    }

    public Iterable<Producto> obtenerTodosLosProductos() {
        return productoRepository.findAll();
    }
}
