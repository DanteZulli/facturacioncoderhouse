package com.zullid.abmclientes.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import org.springframework.stereotype.Service;

@Service
public class FechaServiceREST {

    public LocalDate obtenerFechaActual() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://worldclockapi.com/api/json/utc/now"))
                .build();

        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() == 200) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.body());
                
                String currentDateTime = jsonNode.get("currentDateTime").asText();
                
                return LocalDate.parse(currentDateTime.substring(0, 10));
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return LocalDate.now();
    }
}
