package com.zullid.abmclientes.service;

import com.zullid.abmclientes.entity.Venta;
import com.zullid.abmclientes.entity.VentaProducto;
import com.zullid.abmclientes.repository.VentaProductoRepository;
import com.zullid.abmclientes.repository.VentaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VentaService {

    @Autowired
    private VentaRepository ventaRepository;

    @Autowired
    private VentaProductoRepository ventaProductoRepository;

    public Venta guardarVenta(Venta venta) {
        return ventaRepository.save(venta);
    }

    public void guardarVentaProductos(List<VentaProducto> ventaProductos) {
        ventaProductoRepository.saveAll(ventaProductos);
    }
}
