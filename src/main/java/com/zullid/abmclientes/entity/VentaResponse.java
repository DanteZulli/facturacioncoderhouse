package com.zullid.abmclientes.entity;

import java.util.List;
import lombok.Data;

@Data
public class VentaResponse {

    private Venta venta;
    private List<VentaProducto> ventaProductos;
}