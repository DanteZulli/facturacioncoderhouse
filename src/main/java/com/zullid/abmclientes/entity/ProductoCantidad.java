package com.zullid.abmclientes.entity;

import lombok.Data;

@Data
public class ProductoCantidad {

    private int idProducto;
    private int cantidad;
}
