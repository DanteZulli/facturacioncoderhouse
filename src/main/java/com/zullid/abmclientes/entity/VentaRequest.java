package com.zullid.abmclientes.entity;

import java.util.List;
import lombok.Data;

@Data
public class VentaRequest {

    private int idCliente;
    private List<ProductoCantidad> productos;
}