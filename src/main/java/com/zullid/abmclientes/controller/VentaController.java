package com.zullid.abmclientes.controller;

import com.zullid.abmclientes.entity.VentaRequest;
import com.zullid.abmclientes.entity.VentaResponse;
import com.zullid.abmclientes.service.ComprobanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ventas")
public class VentaController {

    @Autowired
    private ComprobanteService comprobanteService;
    
    @PostMapping("/realizar")
    public VentaResponse realizarVenta(@RequestBody VentaRequest ventaRequest) {
        return comprobanteService.generarComprobante(ventaRequest);
    }

}


