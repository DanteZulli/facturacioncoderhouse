package com.zullid.abmclientes.controller;

import com.zullid.abmclientes.entity.Producto;
import com.zullid.abmclientes.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/productos")
public class ProductoController {
    
    @Autowired
    private ProductoService productoService;
    
    @PostMapping("/agregar")
    public @ResponseBody String agregarProducto(@RequestBody Producto nuevoProducto){
        return productoService.agregarProducto(nuevoProducto);
    }
    
    @PostMapping("/eliminar/{id}")
    public @ResponseBody String eliminarProducto(@PathVariable int id){
        return productoService.eliminarProducto(id);
    }

    @GetMapping("/obtener/{id}")
    public @ResponseBody Producto obtenerProductoPorId(@PathVariable int id){
        return productoService.obtenerProductoPorId(id);
    }
    
    @PostMapping("/actualizar/{id}")
    public @ResponseBody String actualizarProducto(@PathVariable int id, @RequestBody Producto productoActualizado){
        return productoService.actualizarProducto(id, productoActualizado);
    }
    
    @GetMapping("/todos")
    public @ResponseBody Iterable<Producto> obtenerTodosLosProductos(){
        return productoService.obtenerTodosLosProductos();
    }
}
