package com.zullid.abmclientes.controller;

import com.zullid.abmclientes.entity.Cliente;
import com.zullid.abmclientes.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/clientes")
public class ClienteController {
    
    @Autowired
    private ClienteService clienteService;
    
    @PostMapping("/agregar")
    public @ResponseBody String agregarCliente(@RequestBody Cliente nuevoCliente){
        return clienteService.agregarCliente(nuevoCliente);
    }
    
    @PostMapping("/eliminar/{id}")
    public @ResponseBody String eliminarCliente(@PathVariable int id){
        return clienteService.eliminarCliente(id);
    }

    @GetMapping("/obtener/{id}")
    public @ResponseBody Cliente obtenerClientePorId(@PathVariable int id){
        return clienteService.obtenerClientePorId(id);
    }
    
    @PostMapping("/actualizar/{id}")
    public @ResponseBody String actualizarCliente(@PathVariable int id, @RequestBody Cliente clienteActualizado){
        return clienteService.actualizarCliente(id, clienteActualizado);
    }
    
    @GetMapping("/todos")
    public @ResponseBody Iterable<Cliente> obtenerTodosLosClientes(){
        return clienteService.obtenerTodosLosClientes();
    }
    
}
